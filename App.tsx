import React from 'react';
import { StyleSheet, Text, PanResponder, View, PanResponderInstance } from 'react-native';
import { AnimatedCircularProgress } from 'react-native-circular-progress';

const MAX_POINTS = 500;
export default class App extends React.Component<any, any> {
    state = {
        isMoving: false,
        pointsDelta: 0,
        points: 500,
        point: 300,
    };

    _panResponder : PanResponderInstance;
    _circularProgressRef: React.RefObject<AnimatedCircularProgress>;

    constructor(props: Readonly<{}>) {
        super(props);
        this._circularProgressRef = React.createRef();
        this._panResponder = PanResponder.create({});
    }

    render() {
        const fill = (this.state.points / MAX_POINTS) * 100;
        const fills = (this.state.point / MAX_POINTS) * 100;
        return (
            <View style={styles.container} {...this._panResponder.panHandlers}>

                <AnimatedCircularProgress
                    size={120}
                    width={15}
                    backgroundWidth={15}
                    fill={fill}
                    tintColor="#0ECC38"
                    tintColorSecondary="#0ECC38"
                    backgroundColor="#464545"
                    arcSweepAngle={270}
                    rotation={225}
                    lineCap="round"
                />

                <AnimatedCircularProgress
                    size={120}
                    width={15}
                    backgroundWidth={15}
                    fill={fill}
                    tintColor="#E3C763"
                    tintColorSecondary="#E3C763"
                    backgroundColor="#464545"
                    arcSweepAngle={270}
                    rotation={225}
                    lineCap="round"
                />

                <AnimatedCircularProgress
                    size={120}
                    width={15}
                    backgroundWidth={15}
                    fill={fills}
                    tintColor="#5F85DB"
                    tintColorSecondary="#5F85DB"
                    backgroundColor="#464545"
                    arcSweepAngle={270}
                    rotation={225}
                    lineCap="round"
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#152d44',
        padding: 100,
    },
});
